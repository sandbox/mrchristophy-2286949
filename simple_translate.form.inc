<?php

/**
 * Administration Form
 */
function simple_translate_form($form, &$form_state){
    
    $rows = simple_translate_get_rows();
    
    // Hide table if there are no rows
    if(empty($rows)){
        return;
    }
    
    $form['#form_id'] = 'simple_translate_form';
    $form["#token"]=drupal_get_token($form['#form_id']);
    $form['simple_translate'] = array(
        '#prefix' => '<div id="simple_translate">',
        '#suffix' => '</div>',
        '#theme' => 'simple_translate_textfield_table'
    );
    
    for($i = 0; $i < count($rows); $i++){
        
        $form['simple_translate']['strows'][$i][] = array(
                '#id' => 'row-' . $i . '-title',
                '#type' => 'markup',
                '#markup' => $rows[$i]['title']
        );

        $delIds = '';
        $x = 1;
        foreach($rows[$i]['langs'] as $key => $lang){
            
            if($x == 1){
                $delIds .= '?delete[]=' . $lang['id'];
            }else{
                $delIds .= '&delete[]=' . $lang['id'];
            }
            
            
            $form['simple_translate']['strows'][$i][] = array(
                '#id' => 'row-' . $i . 'id-'. $lang['id'] .'-value',
                '#type' => 'textfield', 
                
                '#default_value' => $lang['value'], 
                '#attributes' => array('name' => $lang['id'])
            );
            $x++;
        }
        $form['simple_translate']['strows'][$i]['delete'] = array(
            '#type' => 'markup',
            '#markup' => '<a href=' . $delIds .'>Delete</a>'
        );
    }
    
    $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save Changes'),
  );
  return $form;
}


/**
 * Add another form
 */
function simple_translate_add_form($form, &$form_state){
    
    
   $form['title'] = array(
       '#type' => 'textfield',
       '#required' => TRUE,
       '#title' => 'Add a row'
   );
    
    $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Add a row'),
  );
  return $form;
}


/**
 * Form Submit
 */
function simple_translate_form_submit($form, &$form_state) {
    
  
  foreach($form_state['input'] as $key => $value){
      if(is_numeric($key) && $key > 0){
          db_update('simple_translate')
          ->fields(array('value' => $value))
          ->condition('id', $key, '=')
          ->execute();
      }
  }
  
  drupal_set_message(t('Settings have been saved.'));
}


function simple_translate_add_form_submit($form, &$form_state) {
    
    $langs = language_list();
    
    foreach($langs as $lang){
        db_insert('simple_translate')
        ->fields(array(
            'title' => $form_state['input']['title'],
            'lang' => $lang->language
        ))
        ->execute();
    };
 
  
  drupal_set_message(t('Text added.'));
}

function simple_translate_add_form_validate($form, &$form_state) {
    $string = $form_state['input']['title'];
    
    $result = db_select('simple_translate', 'translate')
    ->fields('translate', array('title'))
    ->condition('title', $string)
    ->execute()
    ->rowCount();
    
    if($result){
         form_set_error('title', 'This text already exists');
    }
    
}